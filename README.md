# README

This README relates to the generation of 12d Macro Language prototypes and the conversion into a suitable format for display.

A description of each file is as follows:

- README.txt - this file
- convert_xml.bat - A batch file used to run all steps and produce the output
- prototypes.css - A Cascading Style Sheet (CSS) for styling the HTML output
- prototypes.js - A Javascript (JS) file containing some advanced functionality for the HTML output
- prototypes.txt - A sample of the prototypes file created by the 12d macro compiler
- prototypes.txt.xml - A sample of the prototypes XML file created by the 12d macro compiler
- prototypes_html.xsl - The XML Stylesheet (XSL) used to transform the XML into HTML output


## Creating the prototype lists

`cc4d.exe -list prototypes.txt`

**cc4d.exe** can be found in the nt.x64 (or nt.x86 for 32-bit installations) directory in the 12d Model installation directory.  **cc4d.exe** is the executable used to compile 12d Macro source code files (\*.4dm) to compiled object code files (\*.4do). By default, this is usually something like: C:\Program Files\12d\12dmodel\14.00

The **cc4d.exe** will automatically create a plain-text file, e.g. prototypes.txt, containing a list of all the 12d Macro Language function prototypes.  The **cc4d.exe** in V10 and above will also automatically create another list of prototypes but in an XML format. 
This will automatically be named with an .xml appended to the specified filename.

e.g. 
`cc4d.exe -list prototypes.txt`
	
will produce **prototypes.txt** and **prototypes.txt.xml**


## Transforming the XML
In order to transform the XML prototypes file into a format we can more easily read, we use an XML Stylesheet (XSL).  In this case, the XSL is called **prototypes_html.xsl**. This is effectively a mapping file to convert XML data to another format.

To produce the resultant HTML output, we need to apply the XSL to the XML and produce an HTML output file.  
This is done using the **XmlTransform.exe** shipped with 12d Model in the **Xml** subdirectory of the Installation path.

This command-line routine takes the XML, XSL and produces an output file.

`XmlTransform.exe source stylesheet output`

source - the source XML file, in our case, prototypes.txt.xml
stylesheet - the XSL file, in our case, prototypes_html.xsl
output - the output fule, in our case, prototypes.html

This is all called via the batch file.  It can also be run via the 12dPL itself or within 12d Model (Reports->Utilities).

## The HTML output
The HTML output contains 2 main parts- a list of all variable/object types found and a table of all the function prototypes. 
The HTML output produced has a few extra features added to it to make it more user friendly:
1. Some basic styling via the CSS file.
2. The ability to sort the table of prototypes by clicking on the header cells.  When sorting, please be patient as there is a lot of data in the table that has to be processed.  This also includes the ability to search/filter the contents of the table.  

All such functionality is included in the prototypes.js file.

prototypes.js - this contains jQuery and DataTables code.
jQuery license - jquery.org/license/
DataTables license - http://www.datatables.net/faqs






