@ECHO OFF
SETLOCAL

REM ========= GENERATE HTML PAGE FOR 12DPL ==========
REM Generates a new list of function prototypes for the 12d 
REM Model Programming Language (12dPL) and creates a local
REM HTML web page based on those functions for easier viewing.
REM 

CLS
ECHO.
ECHO ##########################################################
ECHO #                                                        #
ECHO #          PROTOTYPES TO HTML (LOCAL WEB PAGE)           #
ECHO #                                                        #
ECHO ##########################################################
ECHO.

REM 12d Model Installation directory
REM Modify to suit your version and location
REM This location is used to specify the locations of the 12dPL
REM Preprocessor (cc4d.exe) which generates the prototypes files
REM and the XML Transformer which applys an XSLT to the XML to
REM generate the output HTML file. 
SET TDINSTALL=C:\Program Files\12d\12dmodel\14.00

REM 12dPL Compiler
REM Used to generate the prototypes files
SET CC4D=%TDINSTALL%\nt.x64\cc4d.exe

REM XML Transform executable
REM Used to apply an XSLT to the prototypes XML file to output 
REM the HTML file
SET XMLT=%TDINSTALL%\Xml\XmlTransform.exe

REM Prototypes files to generate
REM Prototypes files to generate- text and XML formats
SET PROTO=prototypes.txt
SET PROTOXML=%PROTO%.xml

REM XML Stylesheet
REM XSLT to use for converting prototypes to HTML file
SET XSL=prototypes_xml_to_html.xslt

REM Output HTML File
REM Name/path of the file to be generated
SET OUTPUT=prototypes.html

IF NOT EXIST "%CC4D%" (
	ECHO.
	ECHO 12dPL Compiler not found. Expected at- %CC4D%
	ECHO Cannot generate prototypes file
	ECHO.
	PAUSE
	EXIT /B 1
) ELSE (
	ECHO.
	ECHO 12dPL Compiler found- %CC4D%
)
IF NOT EXIST "%XMLT%" (
	ECHO.
	ECHO XML Transform executable not found. Expected at- %XMLT%
	ECHO Cannot transform prototypes to HTML file
	ECHO.
	PAUSE
	EXIT /B 2
) ELSE (
	ECHO XML Transform found- %XMLT%
)

IF NOT EXIST "%XSL%" (
	ECHO.
	ECHO HTML XSLT not found. Expected at- %XSL%
	ECHO Cannot transform prototypes to HTML file
	ECHO.
	PAUSE
	EXIT /B 3
) ELSE (
	ECHO HTML XSLT found- %XSL%
)
ECHO.
ECHO Generating prototypes for HTML output...
"%CC4D%" -list "%PROTO%"
IF ERRORLEVEL 1 (
	ECHO.
	ECHO 12dPL Compiler returned an error- %ERRORLEVEL%
	ECHO Problem generating prototypes file- %PROTO%
	ECHO.
	PAUSE
	EXIT /B 4
)

IF NOT EXIST "%PROTOXML%" (
	ECHO.
	ECHO Prototypes XML file not found. Expected at- %PROTOXML%
	ECHO Were there any errors in generating the prototypes?
	ECHO.
	PAUSE
	EXIT /B 5
) ELSE (
	ECHO Prototypes XML created- %PROTOXML%
)

:DO_XSLT
ECHO Transforming prototypes XML to HTML..(please be patient!)
"%XMLT%" "%PROTOXML%" "%XSL%" "%OUTPUT%"
IF ERRORLEVEL 1 (
	ECHO.
	ECHO XML Transform returned an error- %ERRORLEVEL%
	ECHO Problem transforming Prototypes XML to HTML file
	ECHO.
	ECHO XML Transform:  %XMLT%
	ECHO Prototypes XML: %PROTOXML%
	ECHO CodeBlocks XSL: %XSL%
	ECHO Output File:    %OUTPUT%
	ECHO.
	PAUSE
	EXIT /B 6
)

IF NOT EXIST "%OUTPUT%" (
	ECHO.
	ECHO HTML file not found. Expected at- %OUTPUT%
	ECHO.
	PAUSE
	EXIT /B 7
) ELSE (
	ECHO.
	ECHO File created- %OUTPUT%
	ECHO.
)

:FINISHED
ECHO Finished.
PAUSE
EXIT /B 0


:END

