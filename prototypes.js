
/* ### DOCUMENT SPECIFIC CODE ### */

$(document).ready(function() {
$('#functions').DataTable( {
	"paging": false,
	"lengthChange": false,
	"searching": true,
	"ordering": true,
	"info": true,
	"autoWidth": true 
	} );
} );

// $(document).ready(function() {
//   var oTable = $('#functions').dataTable();
//   $('#functions_filter input').unbind('keyup');
//   $('<button>Filter</button>').appendTo('#functions_filter').click( function () {
// 	oTable.fnFilter( $('#functions_filter input').val() );
//   } );
//   $('<button>Reset</button>').appendTo('#functions_filter').click( function () {
// 	oTable.fnFilterClear();
//   } );
// } );

/*
$(document).ready(function() {
	var oTable = $('#functions').dataTable();
	oTable.oLanguage.sProcess = "<div id='spinner' class='spinner' style='display:none;'><img id='img-spinner' src='spinner.gif' alt='Loading'/></div>";
} );
*/

// $.fn.dataTableExt.oApi.fnFilterClear  = function ( oSettings )
// {
//     /* Remove global filter */
//     oSettings.oPreviousSearch.sSearch = "";
//      
//     /* Remove the text of the global filter in the input boxes */
//     if ( typeof oSettings.aanFeatures.f != 'undefined' )
//     {
//         var n = oSettings.aanFeatures.f;
//         for ( var i=0, iLen=n.length ; i<iLen ; i++ )
//         {
//             $('input', n[i]).val( '' );
//         }
//     }
//      
//     /* Remove the search text for the column filters - NOTE - if you have input boxes for these
//      * filters, these will need to be reset
//      */
//     for ( var i=0, iLen=oSettings.aoPreSearchCols.length ; i<iLen ; i++ )
//     {
//         oSettings.aoPreSearchCols[i].sSearch = "";
//     }
//      
//     /* Redraw */
//     oSettings.oApi._fnReDraw( oSettings );
// }


function filter(string)
{
	var oTable = $('#functions').DataTable();
	oTable.fnFilter( string );
}



