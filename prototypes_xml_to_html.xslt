<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" 
	doctype-system="about:legacy-compat"
    indent="yes"
    encoding="utf-8"/>

	<xsl:template match="/">
		<html>
		<head>
			<title>12d Programming Language Prototypes</title>
			<script type="text/javascript" src="jquery-3.3.1.min.js" />
			<script type="text/javascript" src="datatables.min.js" />
			<link rel="stylesheet" type="text/css" href="datatables.min.css" />
			<link rel="stylesheet" type="text/css" href="prototypes.css" />
			<script type="text/javascript" src="prototypes.js" />
		</head>
		<body>
			<h1>12d Programming Language Prototypes</h1>
			<div id="timestamp">
				Generated at <script type="text/javascript">document.write(document.lastModified);</script>
			</div>
			<xsl:call-template name="object_types" />
			<h2>Functions</h2>
			<div id="function_links" ></div>
			<table id="functions" class="display">
				<thead>
					<tr>
						<td>ID</td>
						<td>Return Type</td>	
						<td>Function Name</td>
						<td>Parameters</td>
					</tr>   	        	
				</thead>
				<tbody>
					<xsl:apply-templates select="MacroCalls/MacroCall" />
				</tbody>
			</table>
		</body>
		</html>
	</xsl:template>

	<xsl:template name="object_types">
		<h2>List of Data Types</h2>
		(Click on a data type to filter)
		<div id="datatypes">
			<xsl:element name="ul">
			<xsl:for-each select="//Type[not(.=preceding::Type)]">
				<xsl:sort select="." order="ascending"/>
				<xsl:element name="li">
					<xsl:element name="a">
						<xsl:attribute name="href">#</xsl:attribute>
						<xsl:attribute name="onclick">filter(&apos;<xsl:value-of select="."/>&apos;)</xsl:attribute>
						<xsl:attribute name="class">object_link</xsl:attribute>
						<xsl:attribute name="title">Filter table for &apos;<xsl:value-of select="."/>&apos;</xsl:attribute>
						<xsl:value-of select="."/>
					</xsl:element>
				</xsl:element>
					<!--
						Comment out the xsl:if check and uncomment the
						linebreak text to produce a list of types, one per line
					-->
					<!-- START -->
					<!--
					<xsl:if test="position()!=last()">
							<xsl:text>,</xsl:text>
					</xsl:if>
					-->
					<!-- END -->

					<!-- Uncomment this section to have a linebreak after each type -->
					<!-- START -->
					<!-- <br/> -->
					<!-- END -->
			</xsl:for-each>
			</xsl:element>
		<br />
		</div>
	</xsl:template>

	<xsl:template match="MacroCall">
		<!-- Only does non-internal calls -->
		<xsl:if test="not(starts-with(Name, '__'))">
			<tr>
				<td class="functionID">
					<xsl:value-of select="LibraryID"/>
				</td>
				<td class="returnType">
					<xsl:value-of select="Return/Type"/>
				</td>
				<td class="functionName">
					<xsl:value-of select="Name"/>
				</td>
				<xsl:apply-templates select="Parameters" />
			</tr>
		</xsl:if>
	</xsl:template>

	<xsl:template match="Parameters">
		<td class="parameters">
			<xsl:text>(</xsl:text>
			<xsl:apply-templates select="Parameter" />
			<xsl:text>);</xsl:text>
		</td>
		
	</xsl:template>

	<xsl:template match="Parameter">
		<span class="paramType">
			<xsl:value-of select="Type"/>
		</span>
		<xsl:text disable-output-escaping="yes"> </xsl:text>
		<span class="paramNName">
			<xsl:if test="Reference">
				<xsl:text>&amp;</xsl:text>
			</xsl:if>
			<xsl:value-of select="Name"/>
		</span>
		<xsl:if test="position() != last()">
			<xsl:text>, </xsl:text>
		</xsl:if>
	</xsl:template>



</xsl:stylesheet>


